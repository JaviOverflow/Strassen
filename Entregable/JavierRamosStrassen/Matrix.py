class SquareMatrix:
    def __init__(self, size=None, data=None):
        self.data = data if data else [[0] * size for i in range(size)]
        self.size = len(self.data)

    def __add__(self, b):
        result = SquareMatrix(self.size)
        for row in range(self.size):
            for col in range(b.size):
                result.data[row][col] += self.data[row][col] + b.data[row][col]
        return result
        
    def __sub__(self, b):
        return self + (-b)

    def __mul__(self, b):
        result = SquareMatrix(self.size)
        for arow in range(self.size):
            for bcol in range(b.size):
                for ai, bi in zip(range(self.size), range(b.size)):
                    result.data[arow][bcol] += self.data[arow][ai] * b.data[bi][bcol]
        return result

    def __neg__(self):
        result = SquareMatrix(self.size)
        for row in range(self.size):
            for col in range(self.size):
                result.data[row][col] = -self.data[row][col]
        return result
        

    def __eq__(self, b):
        if self.size == b.size:
            for arow, brow in zip(self.data, b.data):
                for ai, bi in zip(arow, brow):
                    if ai != bi:
                        return False
            return True
        return False

    @staticmethod
    def fromFile(path):
        re = __import__('re')
        data = []
        f = open(path, "r")
        for line in re.split("\n|\r", f.read()):
            if not line: continue
            row = []
            for num in re.split(";", line):
                 row.append(int(num))
            data.append(row)
        return SquareMatrix(data=data)

    def submatrix(self, asrow, ascol, n):
        data = []
        for i in range(n):
            data += [self.data[asrow+i][ascol:ascol+n]]
        return SquareMatrix(data=data)

    def addSubmatrix(self, m, srow, scol, dim):
        for row in range(dim):
            for col in range(dim):
                self.data[srow+row][scol+col] += m.data[row][col]


    @staticmethod
    def mulDAC(a, b, result):
        
        n = a.size

        if n <= SquareMatrix.BASIC_CASE:
            ab = a * b
            for row in range(result.size):
                for col in range(result.size):
                    result.data[row][col] = ab.data[row][col]

        else:
            dim = n//2
            m = SquareMatrix(dim)

            # case M1 = (A11 + A22) * (B11 + B22) ; Add to C11 & C22
            SquareMatrix.mulDAC((a.submatrix(0, 0, dim) + a.submatrix(dim, dim, dim)), \
                (b.submatrix(0, 0, dim) + b.submatrix(dim, dim, dim)), m)
            result.addSubmatrix(m, 0, 0, dim)
            result.addSubmatrix(m, dim, dim, dim)

            # case M2: (A21 + A22) * B11 ; Add to C21 & Substract to C22
            SquareMatrix.mulDAC((a.submatrix(dim, 0, dim) + a.submatrix(dim, dim, dim)),\
                b.submatrix(0, 0, dim), m)
            result.addSubmatrix(m, dim, 0, dim)
            result.addSubmatrix(-m, dim, dim, dim)

            # case M3: A11 * (B12 - B22) ; Add to C12 & C22
            SquareMatrix.mulDAC(a.submatrix(0, 0, dim),\
                (b.submatrix(0, dim, dim) - b.submatrix(dim, dim, dim)), m)
            result.addSubmatrix(m, 0, dim, dim)
            result.addSubmatrix(m, dim, dim, dim)

            # case M4: A22 * (B21 - B11) ; Add to C11 & C21
            SquareMatrix.mulDAC(a.submatrix(dim, dim, dim),\
                (b.submatrix(dim, 0, dim) - b.submatrix(0, 0, dim)), m)
            result.addSubmatrix(m, 0, 0, dim)
            result.addSubmatrix(m, dim, 0, dim)

            # case M5: (A11 + A12) * B22 ; Substract to C11 & Add to C12
            SquareMatrix.mulDAC((a.submatrix(0, 0, dim) + a.submatrix(0, dim, dim)),\
                b.submatrix(dim, dim, dim), m)
            result.addSubmatrix(-m, 0, 0, dim)
            result.addSubmatrix(m, 0, dim, dim)

            # case M6: (A21 - A11) * (B11 + B12) ; Add to C22
            SquareMatrix.mulDAC((a.submatrix(dim, 0, dim) - a.submatrix(0, 0, dim)),\
                (b.submatrix(0, 0, dim) + b.submatrix(0, dim, dim)), m)
            result.addSubmatrix(m, dim, dim, dim)

            # case M7: (A12 - A22) * (B21 + B22) ; Add to C11
            SquareMatrix.mulDAC((a.submatrix(0, dim, dim) - a.submatrix(dim, dim, dim)),\
                (b.submatrix(dim, 0, dim) + b.submatrix(dim, dim, dim)), m)
            result.addSubmatrix(m, 0, 0, dim)

    
    def mul_DAC_Strassen(self, b):
        if self.size != b.size:
            print("Las matrices no tienen el mismo tamaño")
            return None
        
        result = SquareMatrix(self.size)
        SquareMatrix.mulDAC(self, b, result)
        return result


    def lookInside(self):
        sys = __import__('sys')

        for row in self.data:
            for num in row:
                sys.stdout.write("%d " % num)
            print("")



