from Matrix import SquareMatrix
from timeit import timeit
from sys import stdout as o
import time

"""
Funciones para generar números aleatorios con matlab

randi para generar matrices de números aleatorios
    randi (<rango_celdas>, <rango1_matriz>, <rango2_matriz>)
      Ejemplo: randi(99999,1024,1024)

dlmwrite para escribir una matriz en un fichero formato csv
    dlmwrite ( <nombre_fichero>, <variable_matriz>, <separadores>)
      Ejemplo: dlmwrite('ficheroOut.csv', mA, ';')
"""

def main():
    # El siguiente fragmento testea que se multiplique de forma correcta
    o.write("Read mA ..")
    c = SquareMatrix.fromFile("matrices/mC.csv")
    o.write("OK\n")

    o.write("Read mB ..")
    d = SquareMatrix.fromFile("matrices/mD.csv")
    o.write("OK\n")

    o.write("mA * mB = mAB .. in ")
    SquareMatrix.BASIC_CASE = 2
    cd = multiplica(c, d)

    o.write(" and is ")
    sol = SquareMatrix.fromFile("matrices/mCD.csv")
    o.write("%s\n" % (cd == sol))

    # Se testea con diferentes rangos (256, 512, 1024) y diferentes longitudes para casos triviales
    for case in ["8", "9"]:
        o.write("Read m%sa .." % case)
        c = SquareMatrix.fromFile("matrices/m%sa.csv" % case)
        o.write("OK\n")

        o.write("Read m%sb .." % case)
        d = SquareMatrix.fromFile("matrices/m%sb.csv" % case)
        o.write("OK\n")

        for n_basic_case in [4, 8, 16, 32, 64, 128, 256]:
            SquareMatrix.BASIC_CASE = n_basic_case
            print("Basic case: %s" % n_basic_case)
            multiplica(c, d)

def timing(f):
    """Decorator para el muestro del tiempo"""
    def wrap(*args):
        time1 = time.time()
        ret = f(*args)
        time2 = time.time()
        print ("%0.3f s" % (time2-time1))
        return ret
    return wrap

@timing
def multiplica(c, d):
    """ Función que lanza la ejecución del DAC Strassen"""
    return SquareMatrix.mul_DAC_Strassen(c, d)
    

if __name__ == "__main__":
    main()

